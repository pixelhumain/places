<?php

namespace PixelHumain\PixelHumain\modules\places\controllers;

use CommunecterController;
use Yii;

/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CoController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
	        'test'  => \PixelHumain\PixelHumain\modules\places\controllers\actions\TestAction::class,
	        'search'  => \PixelHumain\PixelHumain\modules\places\controllers\actions\SearchAction::class
	    );
	}

	public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        return $this->renderPartial("../default/index");
	    else
    		return $this->render("../default/index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}
}
