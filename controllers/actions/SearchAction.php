<?php

namespace PixelHumain\PixelHumain\modules\places\controllers\actions;
use CAction;

class SearchAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	echo $this->getController()->renderPartial("co2.views.app.search",array("type"=>"place"));
    }
}

