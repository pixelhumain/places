<?php

namespace PixelHumain\PixelHumain\modules\places\controllers\actions;
use CAction;

class TestAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	return $this->getController()->render("index");
    }
}